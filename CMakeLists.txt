cmake_minimum_required(VERSION 2.6.11)
project(ConnectFour)

add_executable(ConnectFour 
	src/Animation.cpp
	src/Animation.h
	src/AnimatedSprite.cpp
	src/AnimatedSprite.h
	src/Color.h
	src/Entity.cpp
	src/Entity.h
	src/Event.h
	src/EventListener.cpp
	src/EventListener.h
	src/EventSystem.cpp
	src/EventSystem.h
	src/Font.cpp
	src/Font.h
	src/Game.cpp
	src/Game.h
	src/GraphicsSystem.cpp
	src/GraphicsSystem.h
	src/InputSystem.cpp
	src/InputSystem.h
	src/main.cpp
	src/MouseEvent.h
	src/Player.cpp
	src/Player.h
	src/Services.cpp
	src/Services.h
	src/Sprite.cpp
	src/Sprite.h
	src/Texture.cpp
	src/Texture.h
	src/Networking.cpp
	src/Networking.h
)

# file (GLOB TEST ${CMAKE_SOURCE_DIR}/src/*)
add_definitions(-DSFML_STATIC)
set(SRC_FILE_DIR ${CMAKE_SOURCE_DIR}/src)

set(GRAPHICS_H ${SRC_FILE_DIR}/Animation.h ${SRC_FILE_DIR}/AnimatedSprite.h ${SRC_FILE_DIR}/Color.h ${SRC_FILE_DIR}/Font.h ${SRC_FILE_DIR}/GraphicsSystem.h ${SRC_FILE_DIR}/Sprite.h ${SRC_FILE_DIR}/Texture.h)
set(GRAPHICS_SRC ${SRC_FILE_DIR}/Animation.cpp ${SRC_FILE_DIR}/AnimatedSprite.cpp ${SRC_FILE_DIR}/Font.cpp ${SRC_FILE_DIR}/GraphicsSystem.cpp ${SRC_FILE_DIR}/Sprite.cpp ${SRC_FILE_DIR}/Texture.cpp)
set(CORE_H ${SRC_FILE_DIR}/game.h)
set(CORE_SRC ${SRC_FILE_DIR}/game.cpp ${SRC_FILE_DIR}/main.cpp)
set(UNIT_H ${SRC_FILE_DIR}/Entity.h ${SRC_FILE_DIR}/Player.h)
set(UNIT_SRC ${SRC_FILE_DIR}/Entity.cpp ${SRC_FILE_DIR}/Player.cpp)
set(EVENTS_H ${SRC_FILE_DIR}/EventListener.h ${SRC_FILE_DIR}/EventSystem.h ${SRC_FILE_DIR}/MouseEvent.h ${SRC_FILE_DIR}/Event.h)
set(EVENTS_SRC ${SRC_FILE_DIR}/EventListener.cpp ${SRC_FILE_DIR}/EventSystem.cpp)
set(SYSTEMS_H ${SRC_FILE_DIR}/InputSystem.h)
set(SYSTEMS_SRC ${SRC_FILE_DIR}/InputSystem.cpp)
set(UTILS_H  ${SRC_FILE_DIR}/Services.h ${SRC_FILE_DIR}/Networking.h)
set(UTILS_SRC ${SRC_FILE_DIR}/Services.cpp ${SRC_FILE_DIR}/Networking.cpp)

source_group(Headers_Files\\Graphics FILES ${GRAPHICS_H})
source_group(Source_Files\\Graphics FILES ${GRAPHICS_SRC})
source_group(Headers_Files\\Core FILES ${CORE_H})
source_group(Source_Files\\Core FILES ${CORE_SRC})
source_group(Headers_Files\\Units FILES ${UNIT_H})
source_group(Source_Files\\Units FILES ${UNIT_SRC})
source_group(Headers_Files\\Systems\\Events FILES ${EVENTS_H})
source_group(Source_Files\\Systems\\Events FILES ${EVENTS_SRC})
source_group(Headers_Files\\Systems FILES ${SYSTEMS_H})
source_group(Source_Files\\Systems FILES ${SYSTEMS_SRC})
source_group(Headers_Files\\Utils FILES ${UTILS_H})
source_group(Source_Files\\Utils FILES ${UTILS_SRC})

include_directories(${CMAKE_SOURCE_DIR}/3rdParty/RakNet/src/ ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/include/)

find_library(
	RAKNET_LIB_DEBUG
	NAMES RakNetLibStatic_d
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/RakNet/lib/debug/
)

find_library(
	RAKNET_LIB_RELEASE
	NAMES RakNetLibStatic
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/RakNet/lib/release/
)

find_library(
	SFML_LIB_GRAPHICS_DEBUG
	NAMES sfml-graphics-s-d
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
 	SFML_LIB_SYSTEM_DEBUG
	NAMES sfml-system-s-d
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
 	SFML_LIB_WINDOW_DEBUG
	NAMES sfml-window-s-d
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	SFML_LIB_GRAPHICS_RELEASE
	NAMES sfml-graphics-s
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
 	SFML_LIB_SYSTEM_RELEASE
	NAMES sfml-system-s
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
 	SFML_LIB_WINDOW_RELEASE
	NAMES sfml-window-s
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	FREETYPE
	NAMES freetype
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	JPEG
	NAMES jpeg
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	OPENAL32
	NAMES openal32
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	FLAC
	NAMES flac
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	VORBISENC
	NAMES vorbisenc
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	VORBISFILE
	NAMES vorbisfile
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	VORBIS
	NAMES vorbis
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

find_library(
	OGG
	NAMES ogg
	HINTS ${CMAKE_SOURCE_DIR}/3rdParty/SFML-2.3.2/lib/
)

target_link_libraries(ConnectFour 
	debug ${RAKNET_LIB_DEBUG} 
	optimized ${RAKNET_LIB_RELEASE}
	Ws2_32.lib
	debug ${SFML_LIB_GRAPHICS_DEBUG}
	optimized ${SFML_LIB_GRAPHICS_RELEASE}
	debug ${SFML_LIB_SYSTEM_DEBUG}
	optimized ${SFML_LIB_SYSTEM_RELEASE}
	debug ${SFML_LIB_WINDOW_DEBUG}
	optimized ${SFML_LIB_WINDOW_RELEASE}
	opengl32.lib
	winmm.lib
	gdi32.lib
	${FREETYPE}
	${JPEG}
	${OPENAL32}
	${FLAC}
	${VORBISENC}
	${VORBISFILE}
	${VORBIS}
	${OGG}
)