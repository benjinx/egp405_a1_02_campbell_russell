#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <memory>
#include "SFML/Graphics/Texture.hpp"

class Texture
{
	friend class GraphicsSystem;
public:
	//Texture(){};
	Texture(int width, int height);
	Texture(const std::string& path);
	~Texture(){};

	// Accessors
	int getWidth()	const { return mWidth; }
	int getHeight()	const { return mHeight; }
	void setWidth(int width) { mWidth = width; }
	void setHeight(int height) { mHeight = height; }
	sf::Texture* getTexture() { return mpTexture.get(); }
	void setTexture(sf::Texture pTexture) { (*mpTexture) = pTexture; }

private:
	int mWidth, mHeight;
	std::shared_ptr<sf::Texture> mpTexture;
};


#endif