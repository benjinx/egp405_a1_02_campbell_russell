#include "Sprite.h"
#include "Texture.h"
#include <assert.h>

Sprite::Sprite()
:mpSprite(new sf::Sprite)
{
}

Sprite::Sprite(Texture* pTexture, const sf::Rect<int>& sourceRect, const sf::Vector2f& origin)
:mpSprite(new sf::Sprite)
{
	mpSprite->setTexture((*pTexture->getTexture()));
	mpSprite->setTextureRect(sourceRect);
	mpSprite->setOrigin(origin);
}

Sprite::Sprite(Texture* pTexture, float sourceX, float sourceY, float width, float height, float originX, float originY)
{
	mpSprite->setTexture((*pTexture->getTexture()));

	//sf::Rect<int> tempRect(sourceX, sourceY, width, height);
	//mpSprite->setTextureRect(tempRect);
	//sf::Rect<float> tempPos(sourceX, sourceY, width, height);
	//mpSprite->setPosition(sourceX, sourceY);

	sf::Vector2f tempOrigin(originX, originY);
	mpSprite->setOrigin(tempOrigin);
}

Sprite::Sprite(Texture* pTexture, float originX, float originY)
{
	//assert(pTexture != NULL);
	mpSprite->setTexture((*pTexture->getTexture()));

	//sf::Rect<int> tempRect(0, 0, pTexture->getSize().x, pTexture->getSize().y);
	//mpSprite->setTextureRect(tempRect);

	sf::Vector2f tempOrigin(originX, originY);
	mpSprite->setOrigin(tempOrigin);
}

void Sprite::init(Texture* pTexture, int sourceX, int sourceY, int width, int height, float originX, float originY)
{
	mpSprite->setTexture((*pTexture->getTexture()));

	sf::Rect<int> tempRect(sourceX, sourceY, width, height);
	mpSprite->setTextureRect(tempRect);

	sf::Vector2f tempOrigin(originX, originY);
	mpSprite->setOrigin(tempOrigin);
}