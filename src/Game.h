#ifndef GAME_H
#define GAME_H

#include <assert.h>
#include <vector>
#include "EventListener.h"
#include "Event.h"
#include "GraphicsSystem.h"
#include "InputSystem.h"
#include "EventSystem.h"
#include "Animation.h"
#include "AnimatedSprite.h"
#include "Entity.h"
#include "Networking.h"




class Game : public EventListener
{
public:
	// Singleton
	static Game* getInstance()		{ assert(mspGameInstance != NULL); return mspGameInstance; }
	static void  initInstance()		{ mspGameInstance = new Game(); }
	static void  cleanupInstance()	{ delete mspGameInstance; }

	bool init();
	void cleanup();
	void gameLoop(RakNet::RakPeerInterface* peer, bool serverOrClient); // false is server, true is client
	void update(sf::Time timeDelta);
	void draw();
	void endGame();

	// Events
	void handleEvent(const Event& theEvent);

	enum GameStates
	{
		StartUp = 0,
		Intro,
		Play,
		Lost,
		Won,
		Pause
	};

	enum Messages
	{
		ID_TTT_TURN_AND_BOARD = ID_USER_PACKET_ENUM,
		ID_TTT_OTHER_USER_WON
	}; unsigned char value_type;

	#pragma pack(push, 1)
	struct Board
	{
		enum Coins { None = 0, Gold, Red }; typedef char move_type;

		bool IsEmpty(int index)
		{
			return mCells[index] == None;
		}

		move_type& operator[](int index)
		{
			return mCells[index];
		}

		unsigned char mPacketID;
		move_type mCells[42];
		bool serverOrClientTurn = false; // false is server, true is client
		int turn = 0;
	};
	#pragma pack(pop)

		Board::move_type  g_mySymbol = Board::None;


		// LOOOOOOOOOOOOOOOOOOOOOP
		unsigned char ProcessTurn(Board& b)
		{
			b.turn++;

			if (b.turn % 2 == 0)
			{
				b.serverOrClientTurn = false;
			}
			else
			{
				b.serverOrClientTurn = true;
			}

			//int cell; // where the token lands
			//std::cout << "\nGimme something kid: " << std::endl;
			//mIsMyTurn = b.serverOrClientTurn;
			//std::cin >> cell;
			
			if (!b.serverOrClientTurn)
			{
				std::cout << "Servers turn now: " << std::endl;
			}
			else
			{
				std::cout << "Clients turn now: " << std::endl;
			}

			std::cout << "mIsMyTurn: " << mIsMyTurn << std::endl;
			
			if (mIsMyTurn)
				mIsMyTurn = false;
			else if (!mIsMyTurn)
				mIsMyTurn = true;


			// Whereever the token was placed is cell;
			//b[cell] = g_mySymbol;

			/*if (b.servOrClient == 0) // HEY ITS THE SERVERS TURN
			{
			//system("pause");
			b.servOrClient = 1;
			}
			else // clients turn
			{
			//system("pause");
			b.servOrClient = 0;
			}*/
			// Store the map to ship off
			

			//if (b.serverOrClient)
			//{

			//}
			//else
			//{
			//} // wait for my turn

			// either return ID_TTT_OTHER_USER_WON; or v
			return ID_TTT_TURN_AND_BOARD;
		}


	GameStates mCurrentState;

private:
	Game();
	~Game();
	static Game* mspGameInstance;

	bool mIsInitialized;
	bool mIsGameRunning;
	bool messageSent = false, mDroppedCoin = false;
	int newIndex;

	//Game
	bool mIsMyTurn;
	Sprite* mActiveCoinSprite;
	std::vector<Board::Coins> mCoinSlots;
	Board::Coins mCoinType;
	bool handlePackets(RakNet::RakPeerInterface* peer);
	bool mCheckForWin(int index, Board::Coins type);

	//Timing
	sf::Time mCurrentTime;
	sf::Time mPreviousTime;
	sf::Time timeDelta;
	sf::Time mEndFrameTime;
	sf::Time mFpsUpdateTime;
	sf::Time mFrameLength;

	sf::Time mTimeGameEnded;
	bool mWonGame;

	int mCurrentFps;
	int	mFrameCounter;

	// Systems
	GraphicsSystem mGraphicsSystem;
	InputSystem mInputSystem;
	EventSystem mEventSystem;                   // Delete pointer stuff in cleanup!
	Networking mNetworkingSystem;

	// temp
	Texture* mpBackgroundTexture;
	Sprite* mpBackgroundSprite;

	Texture* mpPlayFieldTexture;
	Sprite* mpPlayFieldSprite;

	Texture* mpCoinTexture;

	Texture* mpImpTexture;
	Sprite* mpImpSprite;

	Texture* mpBackgroundNoColorTexture;
	Sprite* mpBackgroundNoColorSprite;
	Texture* mpConnect4TitleTexture;
	Sprite* mpConnect4TitleSprite;
	Texture* mpInHellTitleTexture;
	Sprite* mpInHellTitleSprite;
	Texture* mpSplashIntroTexture;
	Sprite* mpSplashIntroSprite;
	Texture* mpSplashLoseTexture;
	Sprite* mpSplashLoseSprite;
	Texture* mpSplashWinTexture;
	Sprite* mpSplashWinSprite;

	AnimatedSprite* mpAnimSp;
	Animation* mpAn;

	sf::Clock clock;

};


unsigned char GetPacketIdentifier(RakNet::Packet* P);

#endif