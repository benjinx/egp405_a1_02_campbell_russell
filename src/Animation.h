#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Text.hpp>
#include <memory>
#include "Texture.h"

class Animation
{
public:

	Animation();
	~Animation(){};

	// Functions
	void addFrame(sf::Rect<int> rect) { mFrames.push_back(rect); }

	// Accessors
	sf::Texture* getSpriteSheet() const { return mpTexture.get(); }
	size_t getSize() { return mFrames.size(); }
	const sf::Rect<int> getFrame(int frame) { return mFrames.at(frame); }

	// Mutators
	void setSpriteSheet(sf::Texture texture) { (*mpTexture) = texture; }

private:
	std::vector<sf::Rect<int>> mFrames;
	std::shared_ptr<sf::Texture> mpTexture;
};

#endif