#include "Networking.h"

bool Networking::launchServer(RakNet::RakPeerInterface* server, int port)
{
	server->SetIncomingPassword(0, 0);

	// IPV4 socket
	RakNet::SocketDescriptor  sd;
	sd.port = port;
	sd.socketFamily = AF_INET;

	if (server->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
	{
		printf("\nFailed to start server with IPV4 ports");
		return false;
	}

	server->SetOccasionalPing(true);
	server->SetUnreliableTimeout(1000);
	server->SetMaximumIncomingConnections(4);

	printf("\nSERVER IP addresses:");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("\n%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
	}

	return true;
}

bool Networking::launchClient(RakNet::RakPeerInterface* client, const char* serverIP, int serverPort, int clientPort)
{
	// IPV4 socket
	RakNet::SocketDescriptor sd(clientPort, 0);
	sd.socketFamily = AF_INET;

	client->Startup(8, &sd, 1);
	client->SetOccasionalPing(true);

	if (client->Connect(serverIP, serverPort, 0, 0) !=
		RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("\nAttempt to connect to server FAILED");
		return false;
	}

	printf("\nCLIENT IP addresses:");
	for (unsigned int i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("\n%i. %s\n", i + 1, client->GetLocalIP(i));
	}

	return true;
}

void Networking::printUsage()
{
	printf("\n-- USAGE --");
	printf("\nTicTacToe <serverPort>");
	printf("\nTicTacToe <serverIP> <serverPort> [clientPort]");
}