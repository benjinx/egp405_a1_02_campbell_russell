#ifndef COLOR_H
#define COLOR_H

#include <SFML/Graphics/Color.hpp>

class Color 
{
public:
	Color(){};
	Color(int r, int g, int b, int a)
	{
		mColor.r = r;
		mColor.g = g;
		mColor.b = b;
		mColor.a = a;
	}
	~Color(){};

	// Accessors
	int getR() { return mColor.r; }
	int getG() { return mColor.g; }
	int getB() { return mColor.b; }
	int getA() { return mColor.a; }

	// Mutators
	void setR(int value) { mColor.r = value; }
	void setG(int value) { mColor.g = value; }
	void setB(int value) { mColor.b = value; }
	void setA(int value) { mColor.a = value; }

private:
	sf::Color mColor;
};

#endif