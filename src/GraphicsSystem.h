#ifndef GRAPHICS_SYSTEM_H
#define GRAPHICS_SYSTEM_H

#include <string>
#include "Color.h"
#include "Sprite.h"
#include "Texture.h"
#include "Font.h"
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Text.hpp>

class GraphicsSystem
{
public:
	GraphicsSystem();
	~GraphicsSystem();

	// Accessors
	int getHeight() { return mHeight; }
	int getWidth()	{ return mWidth; }

	// Control functions
	bool init(const std::string& displayTitle, int displayWidth, int displayHeight);
	void cleanup();
	void clearToColor(Color& color);
	void flipDisplay();

	// Sprites & Textures
	void drawSprite(const sf::Sprite& sprite, const sf::Rect<int> destinationRect, bool shouldFlip = false);
	void drawColoredSprite(const sf::Sprite& sprite, const sf::Rect<int> rect, Color& color, bool shouldFlip = false);
	// Text
	void writeText(const std::string& message, int size, Color& color, sf::Vector2f& location, bool shouldFlip = false);
	void writeText(const std::string& message, int size, Color& color, int x, int y, bool shouldFlip = false);

	// Geometry
	//void drawLine(const Vector2f& end1, const Vector2f& end2, const Color& color);
	//void fillRect(const Rect& rect, const Color& color);
	//void drawRect(const Rect& rect, const Color& color);

	sf::RenderWindow* getWindow() { return mpWindow; }

private:
	//std::shared_ptr<T> mpWindow(new sf::RenderWindow());
	sf::RenderWindow* mpWindow;
	//std::shared_ptr<std::string> testing(new std::string("testing"));
	//auto ptr = std::make_shared<int>(1);
	//std::unique_ptr<Font> mpFont;
	std::shared_ptr<sf::Texture> mpTexture;
	int	mWidth, mHeight;
	bool mIsInitialized;
};

#endif GRAPHICS_SYSTEM_H