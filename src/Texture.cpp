#include "Texture.h"

Texture::Texture(int width, int height)
:mpTexture(new sf::Texture)
,mWidth(width)
,mHeight(height)
{
	mpTexture->create(mWidth, mHeight);
}

Texture::Texture(const std::string& path)
:mpTexture(new sf::Texture)
{
	mpTexture->loadFromFile(path);
	mWidth = mpTexture->getSize().x;
	mHeight = mpTexture->getSize().y;
}
