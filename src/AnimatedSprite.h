#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include "Animation.h"
#include "Color.h"
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <memory>

class AnimatedSprite
{
public:
	AnimatedSprite(sf::Time frameTime = sf::seconds(0.2f), bool paused = false, bool isLooping = true);
	~AnimatedSprite(){};

	void update(sf::Time deltaTime);
	void setAnimation(const Animation& animation);
	void setFrameTime(sf::Time frameTime) { mFrameTime = frameTime; }
	void play() { mIsPaused = false; }
	void play(const Animation& animation);
	void pause() { mIsPaused = true; }
	void stop();
	void setIsLooping(bool looping) { mIsLooping = looping; }
	void setColor(const sf::Color& color);
	const std::shared_ptr<Animation> getAnimation() { return mAnimation; }
	const sf::Rect<float> getLocalBounds();
	//const sf::Rect<float> getGlobalBounds() { return getTransform().transformRect(getLocalBounds()); }
	const bool isLooping() { return mIsLooping; }
	const bool isPlaying() { return !mIsPaused; }
	const sf::Time getFrameTime() { return mFrameTime; }
	void setFrame(int newFrame, bool resetTime = true);
	void draw(sf::Sprite* sprite);

private:
	const std::shared_ptr<Animation> mAnimation;
	sf::Time mFrameTime;
	sf::Time mCurrentTime;
	int mCurrentFrame;
	bool mIsPaused;
	bool mIsLooping;
	const std::shared_ptr<sf::Texture> mTexture;
	sf::Vertex mVertices[4];
};

#endif ANIMATED_SPRITE_H