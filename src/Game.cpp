#include "Game.h"
#include "Services.h"
#include <ctime>
#include "RakSleep.h"




Game* Game::mspGameInstance = NULL;

Game::Game() :
	EventListener(),
	mIsInitialized(false),
	mIsGameRunning(false)
{
	mEventSystem.addListener(EVENT_GAME_EXIT, this);
	mEventSystem.addListener(MOVE_LEFT, this);
	mEventSystem.addListener(MOVE_RIGHT, this);
	mEventSystem.addListener(DROP_COIN, this);
	mEventSystem.addListener(DEBUG, this);

	setRemoveOnDestruct(false); // Game shouldn't auto remove itself from the event system.
}

Game::~Game()
{
	cleanup();
}

bool Game::init()
{
	if (mIsInitialized)
		return false;

	int displayWidth = 960, displayHeight = 540;
	
	// Initalize the systems.
	if (!mGraphicsSystem.init("Connect Four in Hell", displayWidth, displayHeight))
		return false;
	if (!mInputSystem.init())
		return false;

	// Provide systems to the service locator.
	Services::init();
	Services::provideGraphicsSystem(&mGraphicsSystem);
	Services::provideInputSystem(&mInputSystem);
	Services::provideEventSystem(&mEventSystem);
	Services::provideNetworkingSystem(&mNetworkingSystem);

	// Timing
	mCurrentTime = sf::seconds(0.0f);
	mPreviousTime = sf::seconds(0.0f);
	mEndFrameTime = sf::seconds(0.0f);
	mFpsUpdateTime = sf::seconds(0.0f);
	mFrameLength = sf::seconds(1.0f / 60.0f);

	mTimeGameEnded = sf::seconds(0.0f);
	mWonGame = false;
	newIndex = -1;

	// Make Change so each player has their own color!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	mCoinType = Board::Coins::Gold;

	// Current State
	mCurrentState = StartUp;
	
	mIsMyTurn = false;

	const std::string PATH = "../assets/";

	mpBackgroundTexture = new Texture(PATH + "Background.png");
	mpBackgroundSprite = new Sprite();
	mpBackgroundSprite->setTexture(mpBackgroundTexture);

	mpPlayFieldTexture = new Texture(PATH + "PlayField.png");
	mpPlayFieldSprite = new Sprite();
	mpPlayFieldSprite->setTexture(mpPlayFieldTexture);
	mpPlayFieldSprite->setPosition((float)displayWidth/2 - mpPlayFieldSprite->getWidth()/2, (float)displayHeight - mpPlayFieldSprite->getHeight());

	mpBackgroundNoColorTexture = new Texture(PATH + "BackgroundNoColor.png");
	mpBackgroundNoColorSprite = new Sprite();
	mpBackgroundNoColorSprite->setTexture(mpBackgroundNoColorTexture);
	mpBackgroundNoColorSprite->setPosition(sf::Vector2f(0.0f, 0.0f));

	mpConnect4TitleTexture = new Texture(PATH + "Connect4Title.png");
	mpConnect4TitleSprite = new Sprite();
	mpConnect4TitleSprite->setTexture(mpConnect4TitleTexture);
	mpConnect4TitleSprite->setPosition(sf::Vector2f(240.0f, 20.0f));

	mpInHellTitleTexture = new Texture(PATH + "TitleInHell.png");
	mpInHellTitleSprite = new Sprite();
	mpInHellTitleSprite->setTexture(mpInHellTitleTexture);
	mpInHellTitleSprite->setPosition(sf::Vector2f(240.0f, 140.0f));

	mpSplashIntroTexture = new Texture(PATH + "SplashIntro.png");
	mpSplashIntroSprite = new Sprite();
	mpSplashIntroSprite->setTexture(mpSplashIntroTexture);
	mpSplashIntroSprite->setPosition(sf::Vector2f(0.0f, 0.0f));

	mpSplashLoseTexture = new Texture(PATH + "SplashLose.png");
	mpSplashLoseSprite = new Sprite();
	mpSplashLoseSprite->setTexture(mpSplashLoseTexture);
	mpSplashLoseSprite->setPosition(sf::Vector2f(0.0f, 0.0f));

	mpSplashWinTexture = new Texture(PATH + "SplashWin.png");
	mpSplashWinSprite = new Sprite();
	mpSplashWinSprite->setTexture(mpSplashWinTexture);
	mpSplashWinSprite->setPosition(sf::Vector2f(0.0f, 0.0f));

	mpImpTexture = new Texture(PATH + "Imp.png");
	mpImpSprite = new Sprite();
	mpImpSprite->setTexture(mpImpTexture);
	mpImpSprite->setSourceSize(100, 80);
	mpImpSprite->setPosition(440, 100);

	mpCoinTexture = new Texture(PATH + "Coins.png");
	mActiveCoinSprite = new Sprite();
	mActiveCoinSprite->setTexture(mpCoinTexture);
	mActiveCoinSprite->setSourceSize(30, 30);
	mActiveCoinSprite->setSourceLocation(90, 0);
	mActiveCoinSprite->setPosition(466, 160);

	for (int i = 0; i < 42; ++i)
	{
		mCoinSlots.push_back(Board::Coins::None);
	}

	mpAn = new Animation();
	mpAn->setSpriteSheet(*mpImpTexture->getTexture());
	mpAn->addFrame(sf::Rect<int>(0, 0, 100, 80));
	mpAn->addFrame(sf::Rect<int>(100, 0, 100, 80));
	mpAn->addFrame(sf::Rect<int>(200, 0, 100, 80));
	mpAn->addFrame(sf::Rect<int>(300, 0, 100, 80));
	mpAnimSp = new AnimatedSprite();
	mpAnimSp->setAnimation(*mpAn);
	mpAnimSp->setFrameTime(sf::Time(sf::seconds(0.15f)));
	mpAnimSp->play();

	mIsInitialized = true;
	return true;
}

void Game::cleanup()
{
	if (mIsInitialized)
	{
		delete mActiveCoinSprite;
		
		//much more

		// Shutdown systems.
		mGraphicsSystem.cleanup();
		

		Services::cleanup();

		mIsInitialized = false;
	}
}

void Game::gameLoop(RakNet::RakPeerInterface* peer, bool serverOrClient) // false is server, true is client
{
	// Begin game w/ intro here or go into loop
	mIsGameRunning = true;


	if (serverOrClient == false) // Set server to be player one
		mIsMyTurn = true;


	sf::Clock frameClock;
	mCurrentFps = 60;

	// Main game loop.
	while (mGraphicsSystem.getWindow()->isOpen() && mIsGameRunning && handlePackets(peer))
	{

		// Time Management
		mCurrentTime = frameClock.getElapsedTime();
		timeDelta = mCurrentTime - mPreviousTime;

		// FrameRate
		mFpsUpdateTime += timeDelta;

		if (mFpsUpdateTime >= sf::seconds(1.0f))
		{
			mFpsUpdateTime -= sf::seconds(1.0f);
			mCurrentFps = mFrameCounter;
			mFrameCounter = 0;

			if (mCurrentFps < 0)
				mCurrentFps = 60;
		}

		mInputSystem.pollInput();
		update(timeDelta);
		draw();

		// FrameRate
		++mFrameCounter;

		mPreviousTime = mCurrentTime;

		//-------Sleep Until Next Frame-------//
		mEndFrameTime = frameClock.getElapsedTime();
		if (mEndFrameTime - mCurrentTime < mFrameLength)
		{
			sf::sleep((mCurrentTime + mFrameLength) - mEndFrameTime);
		}

		RakSleep(30);
	}
}

void Game::endGame()
{
	mIsGameRunning = false;
}

void Game::update(sf::Time timeDelta)
{
	switch (mCurrentState)
	{
	case StartUp:
		if (mCurrentTime > sf::seconds(5.0f))
			mCurrentState = Intro;
		break;
	case Intro:
		if (mCurrentTime > sf::seconds(10.0f))
			mCurrentState = Play;
		break;
	case Play:
		mpAnimSp->update(timeDelta);

		if (mTimeGameEnded != sf::seconds(0.0f) && mCurrentTime > mTimeGameEnded + sf::seconds(3.0f))
		{
			if (mWonGame)
				mCurrentState = Won;
			else
				mCurrentState = Lost;
		}
		break;
	case Lost:
		break;
	case Won:
		break;
	}
}

void Game::draw()
{
	// Draw
	sf::Rect<int> temp(0, 0, 0, 0);
	Sprite* tmpSprite = new Sprite();

	mGraphicsSystem.drawSprite(mpBackgroundSprite->getSprite(), temp);

	switch (mCurrentState)
	{
	case StartUp:

		if (mCurrentTime < sf::seconds(2.0f))
			mGraphicsSystem.drawSprite(mpBackgroundNoColorSprite->getSprite(), temp);
		else
			mGraphicsSystem.drawSprite(mpInHellTitleSprite->getSprite(), temp);

		mGraphicsSystem.drawSprite(mpConnect4TitleSprite->getSprite(), temp);
		break;

	case Intro:

		mGraphicsSystem.drawSprite(mpSplashIntroSprite->getSprite(), temp);
		break;

	case Pause:

		break;

	case Play:

		mGraphicsSystem.drawSprite(mpPlayFieldSprite->getSprite(), temp);

		if (mTimeGameEnded == sf::seconds(0.0f)) // If game not ending
		{
			mpAnimSp->draw(&mpImpSprite->getSprite());

			// Set active coin sprite
			if (mIsMyTurn)
			{
				if (mCoinType == Board::Coins::Gold)
					mActiveCoinSprite->setSourceLocation(90, 0);
				else
					mActiveCoinSprite->setSourceLocation(90, 30);
			}
			else
			{
				if (mCoinType == Board::Coins::Gold)
					mActiveCoinSprite->setSourceLocation(90, 30);
				else
					mActiveCoinSprite->setSourceLocation(90, 0);
			}
			mGraphicsSystem.drawSprite(mActiveCoinSprite->getSprite(), temp);
		}
		else // If game is ending (show final board)
		{
			mGraphicsSystem.drawSprite(mpBackgroundNoColorSprite->getSprite(), temp);
			mGraphicsSystem.drawSprite(mpPlayFieldSprite->getSprite(), temp);
		}

		tmpSprite->setTexture(mpCoinTexture);
		tmpSprite->setSourceSize(30, 30);

		for (int i = 0; i < 42; ++i)
		{
			if (mCoinSlots[i] != Board::Coins::None)
			{
				if (mCoinSlots[i] == Board::Coins::Gold)
					tmpSprite->setSourceLocation(90, 0);
				else
					tmpSprite->setSourceLocation(90, 30);

				tmpSprite->setPosition((float)339 + (42 * (i % 7)), (float)204 + (42 * (i / 7)));
				mGraphicsSystem.drawSprite(tmpSprite->getSprite(), temp);
			}
		}

		// Draw your own coin type at the bottom
		if (mCoinType == Board::Coins::Gold)
			tmpSprite->setSourceLocation(90, 0);
		else
			tmpSprite->setSourceLocation(90, 30);

		tmpSprite->setPosition((float)466, (float)490);
		mGraphicsSystem.drawSprite(tmpSprite->getSprite(), temp);
		break;

	case Lost:
		mGraphicsSystem.drawSprite(mpSplashLoseSprite->getSprite(), temp);
		break;

	case Won:
		mGraphicsSystem.drawSprite(mpSplashWinSprite->getSprite(), temp);
		break;
	}

	delete tmpSprite;

	// Present the display and clear it
	mGraphicsSystem.flipDisplay();
}

bool Game::mCheckForWin(int index, Board::Coins type) // index of new coin
{
	int checkStart = 0;
	bool foundWin = false;
	std::vector<Board::Coins> tmpCheck;

	for (int i = 0; i < 4; ++i)
		tmpCheck.push_back(Board::Coins::None);

	// Check vertically down
	if ((index + 7 < 42) && mCoinSlots[index + 7] == type)
	{
		if ((index + 14 < 42) && mCoinSlots[index + 14] == type)
		{
			if ((index + 21 < 42) && mCoinSlots[index + 21] == type)
			{
				foundWin = true;
			}
		}
	}

	// Check horizontally
	if (!foundWin)
	{
		checkStart = (index / 7) * 7; // Left most of the concerned row

		for (int i = 0; i < 7 && !foundWin; ++i)
		{
			// Fill tmpCheck
			if (i < 4)
			{
				tmpCheck[i] = mCoinSlots[checkStart + i];
			}
			else
			{
				// Push coin slots back 1 to make room for checking to the right
				tmpCheck[0] = tmpCheck[1];
				tmpCheck[1] = tmpCheck[2];
				tmpCheck[2] = tmpCheck[3];
				tmpCheck[3] = mCoinSlots[checkStart + i];
			}

			// Check for win
			if (i > 2)
			{
				if (tmpCheck[0] == type && tmpCheck[1] == type && tmpCheck[2] == type && tmpCheck[3] == type)
				{
					foundWin = true;
				}
			}
		}
	}

	// Check diagonally down-left
	if (!foundWin)
	{
		if (index > 2 && index != 7 && index != 8 && index != 14 && index < 39 && index != 33 && index != 34 && index != 27) // When not to bother
		{
			for (int i = 0; i < 4; ++i)
				tmpCheck[i] = Board::Coins::None;

			// Find Top Right of concerned diagonal line
			checkStart = index;
			while (checkStart - 6 > -1 && checkStart != 13 && checkStart != 20)
			{
				checkStart -= 6;
			}

			for (int i = 0; i < 6 && !foundWin && (checkStart + (i * 6) < 39); ++i)
			{
				// Fill tmpCheck
				if (i < 4)
				{
					tmpCheck[i] = mCoinSlots[checkStart + (i * 6)];
				}
				else
				{
					// Push coin slots back 1 to make room for checking to the right
					tmpCheck[0] = tmpCheck[1];
					tmpCheck[1] = tmpCheck[2];
					tmpCheck[2] = tmpCheck[3];
					tmpCheck[3] = mCoinSlots[checkStart + (i * 6)];
				}

				// Check for win
				if (i > 2)
				{
					if (tmpCheck[0] == type && tmpCheck[1] == type && tmpCheck[2] == type && tmpCheck[3] == type)
					{
						foundWin = true;
					}
				}
			}
		}
	}

	// Check diagonally down-right
	if (!foundWin)
	{
		if (index != 4 && index != 5 && index != 6 && index != 12 && index != 13 && index != 20 && index != 21 && index != 28 && index != 29 && index != 35 && index != 36 && index != 37) // When not to bother
		{
			for (int i = 0; i < 4; ++i)
				tmpCheck[i] = Board::Coins::None;

			// Find Top Left of concerned diagonal line
			checkStart = index;
			while (checkStart - 8 > -1 && checkStart != 7 && checkStart != 14)
			{
				checkStart -= 8;
			}

			for (int i = 0; i < 6 && !foundWin && (checkStart + (i * 8) < 42); ++i)
			{
				// Fill tmpCheck
				if (i < 4)
				{
					tmpCheck[i] = mCoinSlots[checkStart + (i * 8)];
				}
				else
				{
					// Push coin slots back 1 to make room for checking to the right
					tmpCheck[0] = tmpCheck[1];
					tmpCheck[1] = tmpCheck[2];
					tmpCheck[2] = tmpCheck[3];
					tmpCheck[3] = mCoinSlots[checkStart + (i * 8)];
				}

				// Check for win
				if (i > 2)
				{
					if (tmpCheck[0] == type && tmpCheck[1] == type && tmpCheck[2] == type && tmpCheck[3] == type)
					{
						foundWin = true;
					}
				}
			}
		}
	}

	return foundWin;
}

void Game::handleEvent(const Event& theEvent)
{
	switch (theEvent.getType())
	{
		case EVENT_GAME_EXIT:
			endGame();
			break;														//
		case MOVE_LEFT:               // If Not waiting for game to end V
			if (mCurrentState == Play && mTimeGameEnded == sf::seconds(0.0f) && mIsMyTurn && mpImpSprite->getPositionX() > 320)
			{
				mpImpSprite->setPosition(mpImpSprite->getPositionX() - 42, mpImpSprite->getPositionY());
				mActiveCoinSprite->setPosition(mActiveCoinSprite->getPosition().x - 42, mActiveCoinSprite->getPosition().y);

				// Send Raknet message to other player

			}
			break;													    //
		case MOVE_RIGHT:              // If Not waiting for game to end V
			if (mCurrentState == Play && mTimeGameEnded == sf::seconds(0.0f) && mIsMyTurn && mpImpSprite->getPositionX() < 560)
			{
				mpImpSprite->setPosition(mpImpSprite->getPositionX() + 42, mpImpSprite->getPositionY());
				mActiveCoinSprite->setPosition(mActiveCoinSprite->getPosition().x + 42, mActiveCoinSprite->getPosition().y);
			
				// Send Raknet message to other player
			}
			break;
		case DROP_COIN:
			if (mCurrentState == StartUp || mCurrentState == Intro)
			{
				mCurrentState = Play;
			}
			else if (mCurrentState == Play && mTimeGameEnded == sf::seconds(0.0f))
			{
				if (mIsMyTurn /*and column isn't full*/ && mCoinSlots[(unsigned __int64)(mActiveCoinSprite->getPositionX() - 340) / 42] == Board::Coins::None)
				{

					// Drop coin
					int column = (int)((mActiveCoinSprite->getPositionX() - 340) / 42);
					bool foundSpot = false;

					for (int i = 5; i > -1 && !foundSpot; --i)
					{
						if (mCoinSlots[(i * 7) + column] == Board::Coins::None)
						{
							mCoinSlots[(i * 7) + column] = mCoinType;
							mWonGame = mCheckForWin((i * 7) + column, mCoinType);
							foundSpot = true;
						}
					}

					if (mWonGame)
					{
						mTimeGameEnded = mCurrentTime;
					}


					//mDroppedCoin = true;
					//mIsMyTurn = false;

					//Send Raknet messages to other player
				}
			}
			else
			{
				// In lost or won state
				mIsGameRunning = false;
			}
			break;

		case DEBUG:
			//if (mCoinType == Board::Coins::Gold)
			//	mCoinType = Board::Coins::Red;
			//else
			//	mCoinType = Board::Coins::Gold;
			break;
	}
}


bool Game::handlePackets(RakNet::RakPeerInterface* peer)
{
	for (RakNet::Packet* p = peer->Receive(); p; peer->DeallocatePacket(p), p = peer->Receive())
	{
		auto packetID = GetPacketIdentifier(p);

		switch (packetID)
		{
		case ID_ALREADY_CONNECTED:
		{ printf("\nAlready connected with guid %s", p->guid); break; }
		case ID_CONNECTION_BANNED:
		{ printf("\nWe are banned from this server"); return false;
		}
		case ID_CONNECTION_ATTEMPT_FAILED:
		{ printf("\nConnection to server failed"); return false;
		}
		case ID_INVALID_PASSWORD:
		{ printf("\nPassword invalid"); return false;
		}

			// Handle server packets - of course, we should handle more types of 
			// packets for a more robust server
		case ID_NEW_INCOMING_CONNECTION:
		{
			printf("\nNew incoming connection from %s with GUID %s",
				p->systemAddress.ToString(true), p->guid.ToString());

			// Server is red
			g_mySymbol = Board::Red;
			mCoinType = Board::Coins::Red;
			break;
		}

			// Handle client packets
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			// client is gold
			g_mySymbol = Board::Gold;
			mCoinType = Board::Coins::Gold;
			printf("Connection require ACCEPTED to %s with GUID %s\n",
				p->systemAddress.ToString(true), p->guid.ToString());
			printf("My external address is %s\n",
				peer->GetExternalID(p->systemAddress).ToString(true));

			Board b;
			b.mPacketID = (unsigned char)ID_TTT_TURN_AND_BOARD;

			for (int i = 0; i < 42; ++i)
			{
				b[i] = Board::None; 
			}

			b.serverOrClientTurn = mIsMyTurn;
			// Sends to the server first message.
			peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

			break;
		}

			// handle common messages
		case ID_CONNECTION_LOST:
		{ printf("\nConnection lost from %s", p->systemAddress.ToString(true)); break; }

		case ID_TTT_TURN_AND_BOARD:
		{
			Board b = *(Board*)p->data;
			auto msg = ProcessTurn(b);

			if (msg == ID_TTT_OTHER_USER_WON)
			{
				b.mPacketID = (unsigned char)ID_TTT_OTHER_USER_WON;
				peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				//PrintBoard(b);
				printf("\nWE WON! :)\n"); 
				return false;
			}
			else // no one won and change values
			{

				// Pass whos turn
				b.serverOrClientTurn = mIsMyTurn;
				//for (int i = 0; i < 42; i++)
				//	b.mCells[i] = mCoinSlots.at(i);

				b.mPacketID = (unsigned char)ID_TTT_TURN_AND_BOARD;
				peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			}

			break;
		}

		case ID_TTT_OTHER_USER_WON:
		{
			Board b = *(Board*)p->data;
			mTimeGameEnded = mCurrentTime;
			//PrintBoard(b);

			printf("\nWE LOST! :(\n"); return false;
		}

		default:
		{ printf("\nReceived unhandled packet\n"); }
		}
	}
	return true;
}

unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}