#ifndef FONT_H
#define FONT_H

#include "SFML/Graphics/Font.hpp"
#include <memory>

class Font
{
	friend class GraphicsSystem;
public:
	Font(){};
	//Font(const std::string& path);
	~Font(){};

	//sf::Font getFont() { return (*mpFont); }

private:
	//std::unique_ptr<sf::Font> mpFont;
	std::string mPath;
};
#endif