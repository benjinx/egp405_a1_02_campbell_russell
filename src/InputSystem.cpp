#include "InputSystem.h"
#include "Services.h"
#include "Event.h"
#include "MouseEvent.h"

InputSystem::InputSystem()
:mIsInitialized(false)
{
}


InputSystem::~InputSystem()
{
	cleanup();
}

bool InputSystem::init()
{
	if (mIsInitialized)
		return false;

	mIsInitialized = true;
	return true;
}

void InputSystem::cleanup()
{
	if (mIsInitialized)
	{
		mIsInitialized = false;
	}
}

// Handle any application messages sent after the previous frame.
void InputSystem::pollInput()
{
	sf::Event inputEvent;

	while (Services::getGraphicsSystem()->getWindow()->pollEvent(inputEvent))
	{
		switch (inputEvent.type)
		{
			case sf::Event::Closed: // Window closed
				Services::getGraphicsSystem()->getWindow()->close();
				break;
			case sf::Event::KeyPressed:
				if (inputEvent.key.code == Input::Key::ESCAPE)
				{
					Services::getEventSystem()->fireEvent(Event(EVENT_GAME_EXIT));
				}
				else if (inputEvent.key.code == Input::Key::SPACE)
				{

				}
				else if (inputEvent.key.code == Input::Key::LEFT_ARROW)
				{
					Services::getEventSystem()->fireEvent(Event(MOVE_LEFT));
				}
				else if (inputEvent.key.code == Input::Key::RIGHT_ARROW)
				{
					Services::getEventSystem()->fireEvent(Event(MOVE_RIGHT));
				}
				else if (inputEvent.key.code == Input::Key::DOWN_ARROW)
				{
					Services::getEventSystem()->fireEvent(Event(DROP_COIN));
				}
				else if (inputEvent.key.code == Input::Key::UP_ARROW)
				{
					Services::getEventSystem()->fireEvent(Event(DEBUG));
				}
				break;
			default:
				break;
		}
	}
}