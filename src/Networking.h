#ifndef NETWORKING_H
#define NETWORKING_H

#include "RakPeer.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "MessageIdentifiers.h"
#include <iostream>

class Networking
{

public:
	Networking(){};
	~Networking(){};

	bool launchServer(RakNet::RakPeerInterface* server, int port);
	bool launchClient(RakNet::RakPeerInterface* client, const char* serverIP, int serverPort, int clientPort);
	void printUsage();

};



#endif NETWORKING_H