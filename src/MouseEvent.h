#ifndef MOUSE_EVENT_H
#define MOUSE_EVENT_H

#include "Event.h"
#include <SFML/System/Vector2.hpp>

class MouseEvent : public Event
{
public:
	MouseEvent(EventType type, const sf::Vector2f& position) :
		Event(type),
		mPosition(position)
	{
	}

	// Accessors
	inline sf::Vector2f getLocation() const { return mPosition; }

private:
	sf::Vector2f mPosition;
};

#endif