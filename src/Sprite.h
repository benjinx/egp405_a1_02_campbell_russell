#ifndef SPRITE_H
#define SPRITE_H

#include <memory>
#include <SFML/Graphics/Sprite.hpp>
#include "Texture.h"

// Sprite: a portion of an image.
class Sprite
{
public:
	Sprite();
	Sprite(Texture* pTexture, const sf::Rect<int>& sourceRect, const sf::Vector2f& origin);
	Sprite(Texture* pTexture, float sourceX, float sourceY, float width, float height, float originX = 0.0f, float originY = 0.0f);
	Sprite(Texture* pTexture, float originX = 0.0f, float originY = 0.0f);

	void init(Texture* pTexture, int sourceX, int sourceY, int width, int height, float originX = 0.0f, float originY = 0.0f);

	// Accessors
	const sf::Texture* getTexture() { return mpSprite->getTexture(); }
	inline int getSourceX() const { return mpSprite->getTextureRect().left; }
	inline int getSourceY() const { return mpSprite->getTextureRect().top; }
	inline int getWidth()	const { return mpSprite->getTextureRect().width; }
	inline int getHeight() const { return mpSprite->getTextureRect().height; }
	inline const sf::Vector2f& getOrigin() const { return mpSprite->getOrigin(); }
	inline float getOriginX() const { return mpSprite->getOrigin().x; }
	inline float getOriginY() const { return mpSprite->getOrigin().y; }

	// Mutators.
	void setTexture(Texture* pTexture) { mpSprite->setTexture(*pTexture->getTexture()); }
	inline void	setOrigin(const sf::Vector2f& origin) { mpSprite->setOrigin(origin); }

	void setPosition(float x, float y) { mpSprite->setPosition(x, y); }
	void setPosition(sf::Vector2f position) { mpSprite->setPosition(position); }

	sf::Vector2f getPosition() { return mpSprite->getPosition(); }
	float getPositionX() { return mpSprite->getPosition().x; }
	float getPositionY() { return mpSprite->getPosition().y; }

	inline void	setSourceLocation(int x, int y)	
	{
		sf::Rect<int> tempRect(x, y, mpSprite->getTextureRect().width, mpSprite->getTextureRect().height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setSourceX(int sourceX) 
	{
		sf::Rect<int> tempRect(sourceX, mpSprite->getTextureRect().top, mpSprite->getTextureRect().width, mpSprite->getTextureRect().height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setSourceY(int sourceY)
	{ 
		sf::Rect<int> tempRect(mpSprite->getTextureRect().left, sourceY, mpSprite->getTextureRect().width, mpSprite->getTextureRect().height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setSourceSize(int width, int height)
	{ 
		sf::Rect<int> tempRect(mpSprite->getTextureRect().left, mpSprite->getTextureRect().top, width, height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setWidth(int width) 
	{
		sf::Rect<int> tempRect(mpSprite->getTextureRect().left, mpSprite->getTextureRect().top, width, mpSprite->getTextureRect().height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setHeight(int height)
	{
		sf::Rect<int> tempRect(mpSprite->getTextureRect().left, mpSprite->getTextureRect().top, mpSprite->getTextureRect().width, height);
		mpSprite->setTextureRect(tempRect);
	}

	inline void	setOrigin(float x, float y) { mpSprite->setOrigin(x, y); }
	inline void	centerOrigin() { mpSprite->setOrigin(mpSprite->getOrigin().x * 0.5f, mpSprite->getOrigin().y * 0.5f); }

	sf::Sprite getSprite() { return (*mpSprite); }

private:
	std::shared_ptr<sf::Sprite> mpSprite;
};


#endif