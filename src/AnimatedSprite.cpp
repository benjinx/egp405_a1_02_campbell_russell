#include "AnimatedSprite.h"
#include "Services.h"

AnimatedSprite::AnimatedSprite(sf::Time frameTime, bool paused, bool isLooping)
:mAnimation(new Animation())
,mTexture(new sf::Texture())
{
}

void AnimatedSprite::setAnimation(const Animation& animation)
{
	(*mAnimation) = animation;
	(*mTexture) = (*mAnimation->getSpriteSheet());
	mCurrentFrame = 0;
	setFrame(mCurrentFrame);
}

void AnimatedSprite::play(const Animation& animation)
{
	if (mAnimation.get() != &animation)
		setAnimation(animation);

	play();
}

void AnimatedSprite::stop()
{
	mIsPaused = true;
	mCurrentFrame = 0;
	setFrame(mCurrentFrame);
}

void AnimatedSprite::setColor(const sf::Color& color)
{
	for (int i = 0; i < 4; i++)
		mVertices[i].color = color;
}

sf::Rect<float> const AnimatedSprite::getLocalBounds()
{
	sf::Rect<int> rect = mAnimation->getFrame(mCurrentFrame);

	float width = static_cast<float>(std::abs(rect.width));
	float height = static_cast<float>(std::abs(rect.height));

	return sf::FloatRect(0.f, 0.f, width, height);
}

void AnimatedSprite::setFrame(int newFrame, bool resetTime)
{
	if (mAnimation)
	{
		//calculate new vertex positions and texture coordiantes
		sf::Rect<int> rect = mAnimation->getFrame(newFrame);

		mVertices[0].position = sf::Vector2f(0.f, 0.f);
		mVertices[1].position = sf::Vector2f(0.f, static_cast<float>(rect.height));
		mVertices[2].position = sf::Vector2f(static_cast<float>(rect.width), static_cast<float>(rect.height));
		mVertices[3].position = sf::Vector2f(static_cast<float>(rect.width), 0.f);

		float left = static_cast<float>(rect.left) + 0.0001f;
		float right = left + static_cast<float>(rect.width);
		float top = static_cast<float>(rect.top);
		float bottom = top + static_cast<float>(rect.height);

		mVertices[0].texCoords = sf::Vector2f(left, top);
		mVertices[1].texCoords = sf::Vector2f(left, bottom);
		mVertices[2].texCoords = sf::Vector2f(right, bottom);
		mVertices[3].texCoords = sf::Vector2f(right, top);
	}

	if (resetTime)
		mCurrentTime = sf::Time::Zero;
}

void AnimatedSprite::update(sf::Time deltaTime)
{
	// if not paused and we have a valid animation
	if (!mIsPaused && mAnimation)
	{
		// add delta time
		mCurrentTime += deltaTime;

		// if current time is bigger then the frame time advance one frame
		if (mCurrentTime >= mFrameTime)
		{
			// reset time, but keep the remainder
			mCurrentTime = sf::microseconds(mCurrentTime.asMicroseconds() % mFrameTime.asMicroseconds());

			// get next Frame index
			if (mCurrentFrame + 1 < mAnimation->getSize())
				mCurrentFrame++;
			else
			{
				// animation has ended
				mCurrentFrame = 0; // reset to start

				if (!mIsLooping)
					mIsPaused = true;
			}

			// set the current frame, not reseting the time
			setFrame(mCurrentFrame, false);
		}
	}
}

void AnimatedSprite::draw(sf::Sprite* sprite)
{
	//if (mAnimation && mTexture)
	//{
		//states.transform *= getTransform();
		//states.texture = mTexture.get();
		//target.draw(mVertices, 4, sf::Quads, states);
		sprite->setTextureRect(mAnimation->getFrame(mCurrentFrame));
		Services::getGraphicsSystem()->drawSprite(*sprite, sf::Rect<int> (0,0,0,0));
	//}
}