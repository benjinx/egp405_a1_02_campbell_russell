Connect Four In Hell
By Benjamin Campbell and Benjamin Russell.

BitBucket URL: 
https://bitbucket.org/benjinx/egp405_a1_02_campbell_russell.git

Description:
This battle-for-your-soul-in-Hell themed Connect Four game is fun for the whole
family! Enter a world of life-like cavernous horror as you fight for your soul. Witness
lagoons of lava, slithering stalactites, and an imp named Vanna Red! 

How to play:
	To Start:
		For servers;
		Write in command line "ConnectFour.exe 200"
	
		For clients;
		Write in command line "ConnectFour.exe (Server IP) 200 201"

	Controls:
		Left/Right arrow: Move Vanna Red
      		      Down arrow: Drop a coin / Skip a splashscreen

Unique features:
Our game is made with nothing but the finest authentic assets hand-crafted by our very
own Benjamin Russell in Photoshop. (~6 hours work)